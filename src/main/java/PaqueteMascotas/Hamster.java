/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package PaqueteMascotas;

/**
 *
 * @author Acer
 */
public class Hamster {
private String nombre;
private String tipo;
private int edad;
private double tamaño;

    public String getNombre() {
        return nombre;
    }

    public void setNombre(String nombre) {
        this.nombre = nombre;
    }

    public String getTipo() {
        return tipo;
    }

    public void setTipo(String tipo) {
        this.tipo = tipo;
    }

    public int getEdad() {
        return edad;
    }

    public void setEdad(int edad) {
        this.edad = edad;
    }

    public double getTamaño() {
        return tamaño;
    }

    public void setTamaño(int tamaño) {
        this.tamaño = tamaño;
    }

    public Hamster(String nombre, String tipo, int edad, double tamaño) {
        this.nombre = nombre;
        this.tipo = tipo;
        this.edad = edad;
        this.tamaño = tamaño;
    }


}
