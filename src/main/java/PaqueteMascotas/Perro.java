/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package PaqueteMascotas;

/**
 *
 * @author Acer
 */
public class Perro {
private String nombre;
private String raza;
private double tamaño;
private String color;

    public String getNombre() {
        return nombre;
    }

    public void setNombre(String nombre) {
        this.nombre = nombre;
    }

    public String getRaza() {
        return raza;
    }

    public void setRaza(String raza) {
        this.raza = raza;
    }

    public double getTamaño() {
        return tamaño;
    }

    public void setTamaño(int tamaño) {
        this.tamaño = tamaño;
    }

    public String getColor() {
        return color;
    }

    public void setColor(String color) {
        this.color = color;
    }

    public Perro(String nombre, String raza, double tamaño, String color) {
        this.nombre = nombre;
        this.raza = raza;
        this.tamaño = tamaño;
        this.color = color;
    }


}
